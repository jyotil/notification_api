import express, { Application, Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';

import getInvitation from './controller/getInvitation';
import getInvitationUpdate from './controller/getInvitationUpdate';
import login from './controller/login';
import ensureToken from './controller/ensureToken';

// initialize configuration
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;

const app: Application = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/', (req: Request, res: Response) => {
	res.send('Hello');
});

app.post('/login', login);
app.get('/getInvitation', ensureToken, getInvitation);
app.get('/getInvitationUpdate', ensureToken, getInvitationUpdate);

// start the express server
const ENVIRONMENT = app.get('env');
app.listen(port, () => {
	// tslint:disable-next-line:no-console
	console.log(`server started at http://localhost:${port} in ${ENVIRONMENT} mode`);
});

export default app;
