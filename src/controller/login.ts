import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

const user_data = require('../helper/users.json');

interface user_detail {
	email: string;
	password: string;
}
interface user_info {
	user_id: number;
	first_name: string;
	last_name: string;
	email: string;
	password: string;
}

const login = (req: Request, res: Response) => {
	try {
		const user_detail: user_detail = req.body;
		const matched_user: user_info = user_data.users.find((user_object: user_info) => {
			return user_object.email == user_detail.email && user_object.password == user_detail.password;
		});
		if (matched_user) {
			const token = jwt.sign({ matched_user }, 'crest_data_systems', { expiresIn: 60 * 60 });
			res.json({ ...matched_user, token: token });
		} else {
			res.status(404).json({ error: 'No User Found' });
		}
	} catch (error) {
		res.status(500).json({ error: error });
	}
};

export default login;
