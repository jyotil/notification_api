import { Request, Response } from 'express';

const invitations = require('../helper/invitations.json');

const getInvitation = (req: Request, res: Response) => {
	res.json({ invites: invitations.invites });
};

export default getInvitation;
