import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

const ensureToken = (req: Request, res: Response, next: NextFunction) => {
	const BEARER_HEADER = req.headers['authorization'];
	if (typeof BEARER_HEADER != 'undefined') {
		const BEARER = BEARER_HEADER.split(' ');
		const BEARER_TOKEN = BEARER[1];
		jwt.verify(BEARER_TOKEN, 'crest_data_systems', (err, data) => {
			if (err) {
				res.status(401).json({ error: 'User is not authorize' });
			} else {
				console.log('ensureToken -> data', data);
				next();
			}
		});
	} else {
		res.sendStatus(403);
	}
};

export default ensureToken;
