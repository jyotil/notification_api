import { Request, Response, NextFunction } from 'express';

const invitations_update = require('../helper/invitations_update.json');

const getInvitationUpdate = (req: Request, res: Response) => {
	res.json({ invites: invitations_update.invites });
};

export default getInvitationUpdate;
