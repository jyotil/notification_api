const json_stringify = (object: any): string => {
	return JSON.stringify(object);
};

export { json_stringify };
